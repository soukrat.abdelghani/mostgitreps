import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text } from 'react-native';

function Setting(){
 
  return (
    <View style={styles.container}>
      <Text>Setting screen</Text>
    </View>
  );
};
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems : 'center',
    alignContent : 'center'
  }
});

export default Setting;