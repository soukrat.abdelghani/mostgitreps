import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { StyleSheet, Image } from 'react-native'
import Listrepos from '../Components/Listrepos'
import Setting from '../Components/Setting'

const ListreposStackNavigator = createStackNavigator({
    Listrepos: {
        screen : Listrepos,
        navigationOptions: {
            title : "Trending Repos"
        }
    }
})

const SettingStackNavigator = createStackNavigator({
    Setting: {
        screen : Setting,
        navigationOptions: {
            title : "Settings"
        }
    }
})

//display bottom tabs (you can add more tabs here)
const allTabNavigator = createBottomTabNavigator({
    Trending: {
        screen : ListreposStackNavigator,
        navigationOptions: {
            tabBarIcon: () => {
                return <Image 
                    source={require('../Images/starblue.png')}
                    style={styles.icon}
                />
            }
        }
    },
    Settings: {
        screen : SettingStackNavigator,
        navigationOptions: {
            tabBarIcon: () => {
                return <Image 
                    source={require('../Images/settings.png')}
                    style={styles.icon}
                />
            }
        }
    }
    },
    {
        tabBarOptions: {
            showLabel: true,
            showIcon: true,
            activeBackgroundColor: '#DDDDDD'
        }
    })

const styles = StyleSheet.create({
    icon: {
        width: 25,
        height: 25
    }
})

export default createAppContainer(allTabNavigator)