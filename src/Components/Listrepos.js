import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Image, FlatList, ActivityIndicator } from 'react-native';
import { getReposFromApi, getMoreReposFromApi } from '../Api/api'

function Listrepos(){
  const [data, setData] = useState([])
  const [isLoading, setIsloading] = useState(true)
  const [page, setPage] = useState(1)

  // max total page
  const totalPages = 34

  useEffect(() => {
    //get data of repos from server
      getReposFromApi().then(data => {
        setData(data.items)
        setIsloading(false)
      })

  }, [])

  //load more repos whene scrolling  
  const loadRepos =() =>{
      setIsloading(true)
      getMoreReposFromApi(page+1).then(getdata => {
          setPage(page+1)
          setIsloading(false)
          setData([ ...data, ...getdata.items ])
      })
  }

  const displayLoading =() =>{
    if (isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' color="#0000ff" />
        </View>
      )
    }
  }

  return (
    <View style={styles.container}>
      {data != null ?
     <FlatList
          style={styles.list}
          data={data}
          keyExtractor={(item) => item.id.toString()}
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            if (page < totalPages) {
              // We call the method loadRepos to load more repositories
              loadRepos()
            }
          }}
          renderItem={({item, index}) => (
            <View style={styles.main_container}>
                <View style={styles.content_container}>
                    <Text style={styles.name_text}>{item.name}</Text>
                    <Text style={styles.description_text}>{item.description}</Text>
                    <View style={styles.content_bottom}>
                        <View style={styles.content_owner}>
                            <Image
                            style={styles.image}
                            source={{uri: item.owner.avatar_url}}
                            />
                            <Text style={styles.detail_text}>{item.owner.login}</Text>
                        </View>
                        <View style={styles.content_star}>
                            <Image
                            style={styles.imagestar}
                            source={require('../Images/star.png')}
                            />
                            <Text style={styles.detail_text}>{item.stargazers_count}</Text>
                        </View>
                    </View>
                </View>
            </View>
          )}
        />
        :
        <View style={{alignItems: 'center'}}>
          <Text style={{color: 'orange'}}>No data !</Text>
        </View>
        }
        {displayLoading()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#E3E9ED'
  },
  main_container: {
    margin : 5,
    padding : 5,
    backgroundColor: '#fff',
    flexDirection: 'row'
  },
  image: {
    width: 25,
    height: 25
  },
  imagestar : {
    width: 20,
    height: 20
  },
  content_container: {
    flex: 1,
    margin: 5
  },
  name_text: {
    fontWeight: 'bold',
    fontSize: 16,
    paddingRight: 5,
    marginBottom : 10
  },
  description_text: {
    fontSize: 16,
    paddingRight: 5,
    marginBottom : 10
  },
  content_bottom : {
    flexDirection : 'row',
    justifyContent: 'space-between' 
  },
  content_owner : {
    flex: 4,
    flexDirection : 'row' 
  },
  content_star : {
    flex: 1,
    flexDirection : 'row' 
  },
  detail_text: {
    flex: 1,
    marginTop : 2,
    marginLeft: 4,
    fontSize: 15,
    color: '#666666'
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default Listrepos;
