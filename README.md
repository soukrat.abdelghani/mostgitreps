# React Native List Rpost Demo Application

The demo application that will list the most starred Github repos that were created in the last 30 days.


## Build and run the app

1. Install React Native as described at [https://facebook.github.io/react-native/docs/getting-started.html#content](https://facebook.github.io/react-native/docs/getting-started.html#content)
2. Clone this repository
3. Run `npm install` , all required components will be installed automatically
3. Then run `npx react-native run-android` (for android)


## librarie

react-navigation.
react-navigation.
react-navigation-stack.
react-navigation-tabs.
react-native-screens.
react-native-safe-area-context.
react-native-reanimated.
react-native-gesture-handler.
@react-native-community/masked-view.

You can install all this libraries with this commande.

`npm install --save react-navigation react-navigation-stack react-navigation-tabs react-native-screens react-native-safe-area-context react-native-reanimated @react-native-community/masked-view react-native-gesture-handler`.

note : this libraries already installed with npm install.
