// Api Git Repos

//get repos from api
export function getReposFromApi () {
    const url = 'https://api.github.com/search/repositories?q=created:>2020-10-26&sort=stars&order=desc'
    return fetch(url)
      .then((response) => response.json())
      .catch((error) => console.error(error))
}

//get more repos from api by page
export function getMoreReposFromApi (page) {
    const url = 'https://api.github.com/search/repositories?q=created:>2020-10-26&sort=stars&order=desc&page='+ page
    return fetch(url)
      .then((response) => response.json())
      .catch((error) => console.error(error))
}